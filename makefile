
install: venv
	venv/bin/pip install --upgrade poetry
	if command -v poetry; then :; else \
		sudo ln -s ${PWD}/venv/bin/poetry /usr/local/bin; fi

venv:
	python3 -m ensurepip
	python3 -m venv venv
